import { formatDate } from '@angular/common';
import { DateFormatterPipe } from './date-formatter.pipe';

describe('DateFormatterPipe', () => {
  it('create an instance', () => {
    const pipe = new DateFormatterPipe();
    expect(pipe).toBeTruthy();
  });


  it('should transform date correctly', () => {
    const pipe = new DateFormatterPipe();
    const inputDate = new Date('2023-01-01T12:34:56');
    const transformedDate = pipe.transform(inputDate);
    const expectedDate = formatDate(inputDate, 'YYYY/MM/dd - HH:MM:ss', 'en');
    expect(transformedDate).toEqual(expectedDate);
  });

});
