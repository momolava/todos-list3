import { Todos } from './todos.model';

describe('Todos Interface', () => {
  it('should follow the structure', () => {
    const todos: Todos = {
      id: '1',
      todoText: 'bd Test',
      userName: 'RminTban',
      name:'armin'
    };

    expect(todos).toBeTruthy();
    expect(todos.id).toBe('1');
    expect(todos.todoText).toBe('bd Test');
    expect(todos.userName).toBe('RminTban');
    expect(todos.name).toBe('armin');
    expect(todos.checked).toBeUndefined();
    expect(todos.avatar).toBeUndefined()
  });
});
