import { TestBed } from '@angular/core/testing';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { TodoListService } from './todo-list.service';
import { Todos } from './todos.model';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';

describe('TodoListService', () => {
  let service: TodoListService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [],
    providers: [TodoListService, provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
});

    service = TestBed.inject(TodoListService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get todo list', () => {
    const sampleTodos: Todos[] = [
      { id: '1', userName:'user1' ,  todoText: 'Todo 1', checked: false, name: 'user1' },
      { id: '2', userName:'user2' ,  todoText: 'Todo 2', checked: false, name: 'user12' }
    ];
    service.getTodoList().subscribe(todos => {
      expect(todos).toEqual(sampleTodos);
    });

    const req = httpTestingController.expectOne(service.apiUrl);
    expect(req.request.method).toBe('GET');
    req.flush(sampleTodos);
  });

  it('should add a todo', () => {
    const newTodo: Todos = { id: '3', userName:'user1' ,  todoText: 'New Todo', checked: false , name: 'user3' };
    service.addTodo(newTodo).subscribe(todo => {
      expect(todo).toEqual(newTodo);
    });

    const req = httpTestingController.expectOne(service.apiUrl);
    expect(req.request.method).toBe('POST');
    req.flush(newTodo);
  });

  it('should update a todo', () => {
    const updatedTodo: Todos = { id: '1', userName:'user1' ,  todoText: 'update Todo', checked: false, name: 'user1' };
    service.updateTodo(updatedTodo).subscribe(todo => {
      expect(todo).toEqual(updatedTodo);
    });
    const req = httpTestingController.expectOne(`${service.apiUrl}/${updatedTodo.id}`);
    expect(req.request.method).toBe('PUT');
    req.flush(updatedTodo);
  });

  it('should remove a todo', () => {
    const removedTodo: Todos = { id: '1', userName:'user1' ,  todoText: 'remove Todo', checked: false, name: 'user1' };
    service.removeTodo(removedTodo).subscribe(todo => {
      expect(todo).toEqual(removedTodo);
    });
    const req = httpTestingController.expectOne(`${service.apiUrl}/${removedTodo.id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(removedTodo);
  });
});
