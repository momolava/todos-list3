export interface Todos {
avatar?:File;
checked?:boolean;
id:string;
todoText:string;
userName:string;
name:string;
createdAt?:string;
}