import { formatDate } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormatter',
  standalone: true
})
export class DateFormatterPipe implements PipeTransform {

  transform(value:any): string {
    const date = formatDate(value,'YYYY/MM/dd - HH:MM:ss','en');
    return date;
  }

}
