import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListService } from './todo-list.service';
import { Todos } from './todos.model';
import { FormsModule } from '@angular/forms';
import { DateFormatterPipe } from "./date-formatter.pipe";

@Component({
    selector: 'app-root',
    standalone: true,
    providers: [TodoListService],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    imports: [CommonModule, FormsModule, DateFormatterPipe]
})
export class AppComponent implements OnInit {
  title = 'to-do-list';
  todos: Todos[] = [];

  
  newTodo:Todos = {  // Initiate default value for new entry
    id:'',
    todoText:'',
    userName:'',
    name:''
  };
  
  constructor(public todoService: TodoListService) {}

  ngOnInit() {
    this.getTodos();
  }

  getTodos() {
    this.todoService.getTodoList().subscribe((todos) => {
      this.todoService.setSpinnerOff();
      this.todos = todos.reverse();
    })
  }

  addTodo(newTodo:Todos) {
    if (this.newTodo.avatar && this.newTodo.userName) 
    {
      this.todoService.addTodo(newTodo).subscribe(() => {
        this.todoService.setSpinnerOff();
        this.getTodos(); // Refresh the todo list after adding a new todo
      });
    }
      else 
      {
      alert('avatar and username are required ...')
      }
    }
  

  updateTodoStatus(todo: Todos) {
    this.todoService.updateTodo(todo).subscribe(() => {
      this.todoService.setSpinnerOff();
      this.getTodos(); // Refresh the todo list after updating a todo
    });
  }

  removeTodo(todo: Todos) {
    this.todoService.removeTodo(todo).subscribe(() => {
      this.todoService.setSpinnerOff();
      this.getTodos(); // Refresh the todo list after removing a todo
    });
  }

  avatarDetector(event: any) {
    const avatar: File = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e: any) => {
      const imageUrl = e.target.result;
      console.log('Selected Image URL:', imageUrl);
      this.newTodo.avatar = imageUrl
    };
    reader.readAsDataURL(avatar);
  }
}
