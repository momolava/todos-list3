import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { Todos } from './todos.model';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {
  apiUrl = 'https://6566062feb8bb4b70ef2cbf7.mockapi.io/angularApi/user-details';
  httpProgress = false;

  constructor(private http: HttpClient) { }

  getTodoList(): Observable<Todos[]> {
    this.setSpinnerOn();
    return this.http.get<Todos[]>(this.apiUrl)
      .pipe(
        catchError(this.errorHandler.bind(this))
      );
  }

  addTodo(todo: Todos): Observable<Todos> {
    this.setSpinnerOn();
    return this.http.post<Todos>(this.apiUrl, todo)
      .pipe(
        catchError(this.errorHandler.bind(this))
      );
  }

  updateTodo(todo: Todos): Observable<Todos> {
    this.setSpinnerOn();
    const url = `${this.apiUrl}/${todo.id}`;
    return this.http.put<Todos>(url, todo)
      .pipe(
        catchError(this.errorHandler.bind(this))
      );
  }

  removeTodo(todo: Todos): Observable<Todos> {
    this.setSpinnerOn();
    const url = `${this.apiUrl}/${todo.id}`;
    return this.http.delete<Todos>(url)
      .pipe(
        catchError(this.errorHandler.bind(this))
      );
  }

  setSpinnerOn() {
    this.httpProgress = true;
  }

  setSpinnerOff() {
    this.httpProgress = false;
  }

  errorHandler = (error: HttpErrorResponse): Observable<never> => {
    this.setSpinnerOff();
    return throwError(() => alert(`${error.status} : ${error.statusText}`));
  };
}
