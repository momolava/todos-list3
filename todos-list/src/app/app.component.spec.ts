import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';
import { Todos } from './todos.model';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should be a default todos model value for new todo entry',() =>{
    const defaultValue:Todos = {
      id:'',
      todoText:'',
      userName:'',
      name: ''
    }

    expect(defaultValue).toEqual(app.newTodo);

  })

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have the 'to-do-list' title`, () => {
    expect(app.title).toEqual('to-do-list');
  });

  it('should have a container to present the app', () => {
    const container = fixture.debugElement.query(By.css('.container'));
    expect(container).not.toBeNull();
  });

  it('should have a new todo entry area', () => {
    const newTodoArea = fixture.debugElement.query(By.css('.new-item'));
    expect(newTodoArea).not.toBeNull();
  });

});
